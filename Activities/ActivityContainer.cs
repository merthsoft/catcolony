﻿using CatColony.Defs;
using CatColony.Entities;
using MoreComplexDataStructures;
using System;
using System.Diagnostics;
using System.Linq;

namespace CatColony.Activities {
    class ActivityContainer : Activity {
        public Activity CurrentActivity { get; private set; }
        public Entity Owner { get; private set; }

        private WeightedRandomGenerator<Activity> activities = new WeightedRandomGenerator<Activity>();

        public ActivityContainer(Entity owner, params (Activity activity, long weight)[] activities) : base(null) {
            this.Owner = owner;
            this.activities.SetWeightings(activities.Select(param => new Tuple<Activity, long>(param.activity, param.weight)).ToList());
            CurrentActivity = this.activities.Generate();
            owner.State = CurrentActivity.Title;
        }

        public override bool Update() {
            if (CurrentActivity.Update()) {
                CurrentActivity = activities.Generate();
                Owner.State = CurrentActivity.Title;
                Owner.FrameIndex = 0;
            }
            return false;
        }
    }
}
