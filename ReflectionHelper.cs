﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CatColony {
    static class ReflectionHelper {
        private static readonly Dictionary<string, ConstructorInfo> ConstructorCache = new Dictionary<string, ConstructorInfo>();
        public static Type GetTypeByName(string className)
            => AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes().Where(at => at.Name == className)).FirstOrDefault();

        public static ConstructorInfo GetConstructor(string type, Type[] types) {
            if (!ConstructorCache.ContainsKey(type)) {
                ConstructorCache[type] = GetTypeByName(type).GetConstructor(types);
            }
            return ConstructorCache[type];
        }

        public static T InvokeConstructor<T>(string type, object[] parameters) {
            var types = parameters.SelectArray(p => p.GetType());
            var ctor = GetConstructor(type, types);
            return ctor.Invoke<T>(parameters) ?? default(T);
        }
    }
}
