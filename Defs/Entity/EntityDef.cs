﻿using Microsoft.Xna.Framework.Content;
using MonoGame.Spritesheet;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CatColony.Defs {
    public class EntityDef : Def {
        string type;
        [JsonProperty("type")]
        public string Type {
            get {
                return type ?? "Entity";
            }
            set {
                type = value;
            }
        }

        [JsonProperty("stateFrames")]
        public Dictionary<string, List<FrameDef>> StateFrames { get; set; }

        bool? isBlocking;
        [JsonProperty("isBlocking")]
        public bool? IsBlocking {
            get {
                return isBlocking ?? false;
            }
            set {
                isBlocking = value;
            }
        }

        bool? snapToGrid;
        [JsonProperty("snapToGrid")]
        public bool? SnapToGrid {
            get {
                return snapToGrid ?? false;
            }
            set {
                snapToGrid = value;
            }
        }

        [JsonProperty("assetName")]
        public string AssetName { get; set; }

        int? spriteSize;
        [JsonProperty("spriteSize")]
        public int? SpriteSize {
            get {
                return spriteSize ?? 1;
            }
            set {
                spriteSize = value;
            }
        }

        int? spriteWidth;
        [JsonProperty("spriteWidth")]
        public int? SpriteWidth {
            get {
                return spriteWidth ?? SpriteSize;
            }
            set {
                spriteWidth = value;
            }
        }

        int? spriteHeight;
        [JsonProperty("spriteHeight")]
        public int? SpriteHeight {
            get {
                return spriteHeight ?? SpriteSize;
            }
            set {
                spriteHeight = value;
            }
        }

        float? speed;
        [JsonProperty("speed")]
        public float? Speed {
            get {
                return speed ?? 0;
            }
            set {
                speed = value;
            }
        }

        [JsonProperty("activities")]
        public List<ActivityDef> Activities { get; set; }

        [JsonProperty("tiles")]
        public string[,] Tiles { get; set; }

        [JsonIgnore]
        public GridSheet GridSheet { get; set; }

        public virtual void LoadContentAndInitialize(ContentManager content) {
            GridSheet = content.Load<GridSheet>(AssetName);
            var numSpritesAcross = GridSheet.Texture.Width / GridSheet.SpriteWidth;

            StateFrames.SelectMany(sf => sf.Value).Where(f => f.Id.HasValue).ForEach(f => {
                f.SetRowAndColumnFromId(numSpritesAcross);
            });
        }
    }
}
