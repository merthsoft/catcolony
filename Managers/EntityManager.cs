﻿using CatColony.Defs;
using CatColony.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using static CatColony.ReflectionHelper;

namespace CatColony.Managers {
    public class EntityManager : IManager, IEnumerable<Entity> {
        private Dictionary<string, EntitySetDef> Entities { get; } = new Dictionary<string, EntitySetDef>();
        private List<Entity> ActiveEntities { get; } = new List<Entity>();

        public EntityDef this[string setName, int index]
            => Entities[setName].Entities[index];

        private void Add(Entity entity) {
            if (entity.EntityDef.SnapToGrid.Value) {
                ActiveEntities.RemoveAll(e => e.X == entity.X && e.Y == entity.Y);
            }
            ActiveEntities.Add(entity);

            if (entity.EntityDef.Tiles != null) {
                for (int i = 0; i < entity.EntityDef.SpriteWidth; i++) {
                    for (int j = 0; j < entity.EntityDef.SpriteHeight; j++) {
                        if (entity.EntityDef.Tiles[j, i] != null) {
                            ManagerManager.MapManager[i + (int)entity.X / 32, j + (int)entity.Y / 32, 0] = ManagerManager.TileManager[entity.EntityDef.Tiles[j, i]];
                        }
                    }
                }
            }
        }

        public IEnumerator<Entity> GetEnumerator()
            => ActiveEntities.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => ActiveEntities.GetEnumerator();

        public void Update() {
            ActiveEntities.ForEach(c => c.Update());
            ActiveEntities.RemoveAll(c => c.Remove);
        }

        public void Draw(SpriteBatch spriteBatch) {
            ActiveEntities.OrderBy(c => c.Y + c.EntityDef.SpriteHeight * 32).ThenBy(c => c.X).ForEach(c => c.Draw(spriteBatch));
        }

        private void LoadContent(EntitySetDef entitySetDef, EntityDef entityDef, ContentManager content) {
            entityDef.LoadContentAndInitialize(content);
            if (!Entities.ContainsKey(entitySetDef.Name)) {
                Entities[entitySetDef.Name] = new EntitySetDef { Name = entitySetDef.Name };
            }
            Entities[entitySetDef.Name].Entities.Add(entityDef);
        }

        public void LoadContent(EntitySetDef entitySetDef, ContentManager content) {
            entitySetDef.Entities.ForEach(e => {
                var type = e.Type;
                if (type == null) {
                    LoadContent(entitySetDef, e, content);
                } else {
                    var strongEntity = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(e), GetTypeByName(type + "Def")) as EntityDef;
                    LoadContent(entitySetDef, strongEntity, content);
                }
                e.Activities.ForEach(a => ManagerManager.ActivityManager.LoadContent(a, content));
            });
        }

        public void Add(string setName, string entityName, int x, int y)
            => Add(new Entity(Entities[setName].Entities.OfName(entityName)) { X = x, Y = y });

        public void AddRandomFromSet(string setName, Vector2 position) {
            var entity = GetRandomEntityFromSet(setName);
            entity.X = (int)(position.X - entity.EntityDef.SpriteWidth * 16);
            entity.Y = (int)(position.Y - entity.EntityDef.SpriteHeight * 16);
            Add(entity);
        }

        public Entity GetRandomEntityFromSet(string setName) {
            var def = Entities[setName].Entities.RandomElement();
            var entity = InvokeConstructor<Entity>(def.Type, new object[] { def });
            return entity;
        }

        public void Initialize(Func<IEnumerable<Entity>> contentGenerator) {
            ActiveEntities.Clear();
            foreach (var entity in contentGenerator()) {
                Add(entity);
            }
        }
    }
}
