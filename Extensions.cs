﻿using CatColony.Defs;
using CatColony.Managers;
using Microsoft.Xna.Framework;
using Myra.Utility;
using SharpNoise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CatColony {
    static class Extensions {
        public static double DoubleValue(this ValueChangedEventArgs<float?> e)
            => e.NewValue.HasValue ? (double)e.NewValue : e.OldValue.HasValue ? (double)e.OldValue : 0;

        public static int IntValue(this ValueChangedEventArgs<float?> e)
            => (int)e.DoubleValue();

        public static float Map(this float value, float from1, float to1, float from2, float to2)
            => (value - from1) / (to1 - from1) * (to2 - from2) + from2;

        public static T Or<T>(this T? v, T or) where T : struct
            => v.GetValueOrDefault(or);

        public static int Add(this int lhs, int? rhs)
            => lhs + rhs.GetValueOrDefault();

        public static Rectangle Clone(this Rectangle r,
            int? x = null,
            int? y = null,
            int? width = null,
            int? height = null,
            int? addX = null,
            int? addY = null,
            int? addWidth = null,
            int? addHeight = null)
            => new Rectangle(x.Or(r.X.Add(addX)),
                        y.Or(r.Y.Add(addY)),
                        width.Or(r.Width.Add(addWidth)),
                        height.Or(r.Height.Add(addHeight)));

        public static T Invoke<T>(this ConstructorInfo constructorInfo, object[] parameters)
            => (T)constructorInfo.Invoke(parameters);

        public static TValue SafeGet<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue = default)
            => dictionary?.ContainsKey(key) ?? false ? dictionary[key] : defaultValue;

        public static List<TResult> SelectList<T, TResult>(this IEnumerable<T> set, Func<T, TResult> selector)
            => set.Select(s => selector(s)).ToList();

        public static TResult[] SelectArray<T, TResult>(this IEnumerable<T> set, Func<T, TResult> selector)
            => set.Select(s => selector(s)).ToArray();
        public static TileDef GetTile(this Dictionary<string, TileSetDef> tileDefs, string setName, string tileName)
            => tileDefs[setName].Tiles.FirstOrDefault(t => t.Name == tileName);

        public static T RandomElement<T>(this IEnumerable<T> set)
            => set.ElementAt(ManagerManager.RandomNumberManager.Next(set.Count()));

        public static void ForEach<T>(this IEnumerable<T> set, Action<T> action) {
            foreach (var s in set) {
                action(s);
            }
        }

        public static void ForEach<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> set, Action<TKey, TValue> action) {
            foreach (var s in set) {
                action(s.Key, s.Value);
            }
        }

        public static void ForEach<T>(this T[,,] array, Action<int, int, int, T> action, int startIndexX = 0, int startIndexY = 0, int endIndexXOffset = 0, int endIndexYOffset = 0) where T : new() {
            T[,,] copy = new T[array.GetLength(0), array.GetLength(1), array.GetLength(2)];
            Array.Copy(array, 0, copy, 0, array.Length);
            for (int k = 0; k < array.GetLength(2); k++) {
                for (int j = startIndexY; j < array.GetLength(1) + endIndexYOffset; j++) {
                    for (int i = startIndexX; i < array.GetLength(0) + endIndexXOffset; i++) {
                        action(i, j, k, copy[i, j, k]);
                    }
                }
            }
        }

        public static void Initialize<T>(this T[,,] array, int layer, Func<int, int, T> action) {
            for (int i = 0; i < array.GetLength(0); i++) {
                for (int j = 0; j < array.GetLength(1); j++) {
                    array[i, j, layer] = action(i, j);
                }
            }
        }

        public static TDef OfName<TDef>(this IEnumerable<TDef> set, string name) where TDef : Def
            => set.First(def => def.Name == name);
    }
}
