﻿using Newtonsoft.Json;

namespace CatColony.Defs {
    public class ActivityDef : Def {
        long? weight;
        [JsonProperty("weight")]
        public long? Weight {
            get {
                return weight ?? 1;
            }
            set {
                weight = value;
            }
        }

        string activity;
        [JsonProperty("activity")]
        public string Activity {
            get {
                return activity ?? Name;
            }
            set {
                activity = value;
            }
        }
    }
}
