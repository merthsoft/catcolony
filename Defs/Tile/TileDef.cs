﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;

namespace CatColony.Defs {
    public class TileDef : Def, IEnumerator<FrameDef> {        
        [JsonProperty("frames")]
        public List<FrameDef> Frames { get; set; }

        bool? isBlocking;
        [JsonProperty("isBlocking")]
        public bool? IsBlocking {
            get {
                return isBlocking ?? false;
            }
            set {
                isBlocking = value;
            }
        }

        [JsonIgnore]
        public TileSetDef Parent { get; set; }

        public int FrameIndex { get; private set; } = 0;
        protected int tickCount;
        protected int lastFrameTickCount;
        protected int loopCount;

        public FrameDef Current => Frames[FrameIndex];

        object IEnumerator.Current => (Frames[FrameIndex].Row, Frames[FrameIndex].Column);

        public void Dispose() {
            
        }

        public bool MoveNext() {
            FrameIndex++;
            lastFrameTickCount = tickCount;
            if (FrameIndex >= Frames.Count) {
                FrameIndex = 0;
                loopCount++;
            }

            return true;
        }

        public void Reset() {
            FrameIndex = 0;
        }

        public void Update() {
            tickCount++;
            if (tickCount - lastFrameTickCount == Current.Delay) {
                MoveNext();
            }
        }
    }
}
