﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CatColony.Managers {
    public class RandomNumberManager : IManager {
        private Random Random = new Random();

        public int Next()
            => Random.Next();

        public int Next(int maxValue)
            => Random.Next(maxValue);

        public int Next(int minValue, int maxValue)
            => Random.Next(minValue, maxValue);

        public double NextDouble()
            => Random.NextDouble();
    }
}
