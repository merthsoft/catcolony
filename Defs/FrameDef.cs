﻿using Newtonsoft.Json;

namespace CatColony.Defs {
    public class FrameDef : Def {
        [JsonProperty("row")]
        public int Row { get; set; }

        [JsonProperty("col")]
        public int Column { get; set; }

        [JsonProperty("id")]
        public int? Id { get; set; }

        int delay;
        [JsonProperty("delay")]
        public int Delay {
            get {
                return delay == 0 ? 5 : delay;
            }
            set {
                delay = value;
            }
        }

        public FrameDef() {
        }

        public void SetRowAndColumnFromId(int numSpritesAcross) {
            var id = Id.Value;
            Column = id % numSpritesAcross;
            Row = id / numSpritesAcross;
        }
    }
}
