﻿using CatColony.Defs;
using CatColony.Entities;
using CatColony.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Myra;
using Myra.Graphics2D.UI;
using Myra.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MM = CatColony.Managers.ManagerManager;

namespace CatColony {
    public class ColonySimulatorGame : Game {
        private MouseState previousMouseState;
        private KeyboardState previousKeyboardState;

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        public ColonySimulatorGame() {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreparingDeviceSettings += Graphics_PreparingDeviceSettings;

            Content.RootDirectory = "Content";
            IsMouseVisible = true;

            IsFixedTimeStep = true;
            TargetElapsedTime = TimeSpan.FromMilliseconds(20); // 20 milliseconds, or 50 FPS.
        }

        private void Graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e) {
            DisplayMode displayMode = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode;
            e.GraphicsDeviceInformation.PresentationParameters.BackBufferFormat = displayMode.Format;
            e.GraphicsDeviceInformation.PresentationParameters.BackBufferWidth = (int)(displayMode.Width / 1.25);
            e.GraphicsDeviceInformation.PresentationParameters.BackBufferHeight = (int)(displayMode.Height / 1.25);
        }

        protected override void Initialize() {
            MM.CameraManager.Initialize(GraphicsDevice);
            
            base.Initialize();
        }

        void InitializeUi() {
            MyraEnvironment.Game = this;

            var grid = new Grid {
                RowSpacing = 8,
                ColumnSpacing = 8,
                MaxHeight = 150,
                Height = 150,
            };

            grid.ColumnsProportions.Add(new Proportion(ProportionType.Auto));
            grid.RowsProportions.Add(new Proportion(ProportionType.Auto));

            var row = 0;
            AddNumericUpDown("Seed", grid, ref row, 0, MM.NoiseManager.Seed, 1, (s, e) => { MM.NoiseManager.Seed = e.IntValue(); randomize(); });
            AddNumericUpDown("Frequency", grid, ref row, 0, MM.NoiseManager.Frequency, .05, (s, e) => { MM.NoiseManager.Frequency = e.DoubleValue(); randomize(); });
            AddNumericUpDown("Lacunarity", grid, ref row, 0, MM.NoiseManager.Lacunarity, .05, (s, e) => { MM.NoiseManager.Lacunarity = e.DoubleValue(); randomize(); });
            AddNumericUpDown("Octave count", grid, ref row, 0, MM.NoiseManager.OctaveCount, 1, (s, e) => { MM.NoiseManager.OctaveCount = e.IntValue(); randomize(); });
            AddNumericUpDown("Persistence", grid, ref row, 0, MM.NoiseManager.Persistence, .05, (s, e) => { MM.NoiseManager.Persistence = e.DoubleValue(); randomize(); });

            Desktop.Widgets.Add(grid);
        }

        private void AddNumericUpDown(string label, Grid grid, ref int row, int column, double initialValue, double increment, EventHandler<ValueChangedEventArgs<float?>> valueChanged) {
            grid.Widgets.Add(new Label {
                Id = "label_" + label,
                Text = label,
                GridColumn = column,
                GridRow = row,
            });
            var frequencyButton = new SpinButton {
                GridColumn = column + 1,
                GridRow = row,
                Width = 100,
                Nullable = false,
                Increment = (float)increment,
                Value = (float?)initialValue,
            };
            frequencyButton.ValueChanged += valueChanged;
            grid.Widgets.Add(frequencyButton);
            row++;
        }

        private void FrequencyButtonitem_ValueChanged(object sender, ValueChangedEventArgs<float?> e) {
            MM.NoiseManager.Frequency = (double)e.NewValue;
            randomize();
        }

        protected override void LoadContent() {
            InitializeUi();
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Directory.EnumerateFiles("Data/TileSets/", "*.json", SearchOption.AllDirectories).ForEach(tileSet => {
                var tileSetDef = JsonConvert.DeserializeObject<TileSetDef>(File.ReadAllText(tileSet));
                MM.TileManager.LoadContent(tileSetDef, Content);
            });

            Directory.EnumerateFiles("Data/Entities/", "*.json", SearchOption.AllDirectories).ForEach(entitySet => {
                var entityDef = JsonConvert.DeserializeObject<EntitySetDef>(File.ReadAllText(entitySet));
                MM.EntityManager.LoadContent(entityDef, Content);
            });

            MM.MapManager.Initialize(getRandomGrassTile, tileGenerator);
            MM.EntityManager.Initialize(contentGenerator);
        }

        private IEnumerable<(int x, int y, TileDef)> tileGenerator() {
            for (int i = 0; i < MapManager.Width; i++) {
                for (int j = 0; j < MapManager.Height; j++) {
                    if (MM.NoiseManager[i, j, 0] < 0) {
                        yield return (i, j, getRandomWaterTile());
                    }
                }
            }
        }

        private IEnumerable<Entity> contentGenerator() {
            yield break;
            //for (int i = 0; i < MM.RandomNumberManager.Next(6, 10); i++) {
            //    foreach (var cell in MM.NoiseManager.GetChunkAt(MM.RandomNumberManager.Next(MapManager.Width), MM.RandomNumberManager.Next(MapManager.Height), 0, .5f, 1)) {
            //        var entity = MM.EntityManager.GetRandomEntityFromSet("Trees");
            //        entity.X = cell.x*32;
            //        entity.Y = cell.y*32;
            //        if (!MM.MapManager.EntityIsBlocked(entity)) {
            //            yield return entity;
            //        }
            //    }
            //}
        }

    private TileDef getRandomWaterTile()
            => MM.TileManager["Water", "Water"];

        private TileDef getRandomPathTile()
            => MM.TileManager["Path", "Path"];

        private TileDef getRandomGrassTile(int x, int y)
            => MM.TileManager["Grass", MM.RandomNumberManager.NextDouble() switch {
                var i when i < .05 => 8,
                var i when i < .01 => 7,
                var i when i < .15 => 6,
                var i when i < .20 => 5,
                var i when i < .25 => 4,
                var i when i < .30 => 3,
                var i when i < .35 => 2,
                _ => 0,
            }];

        protected override void UnloadContent() {
            spriteBatch.Dispose();
        }

        protected override void Update(GameTime gameTime) {
            var keyboardState = Keyboard.GetState();
            var mouseState = Mouse.GetState();
            var worldPosition = MM.CameraManager.ScreenToWorld(mouseState.X, mouseState.Y);

            try {
                if (mouseState.LeftButton == ButtonState.Pressed) {
                    if (keyboardState.IsKeyUp(Keys.LeftShift)) {
                        spawnAnimal(worldPosition);
                    } else {
                        spawnTree(worldPosition);
                    }
                } else if (mouseState.RightButton == ButtonState.Pressed) {
                    makeTile(worldPosition, keyboardState.IsKeyDown(Keys.LeftShift), keyboardState.IsKeyDown(Keys.LeftControl));
                }

                //var scrollMagnitude = Math.Abs(mouseState.ScrollWheelValue - previousMouseState.ScrollWheelValue);
                //var scrollDirection = Math.Sign(mouseState.ScrollWheelValue - previousMouseState.ScrollWheelValue);
                //if (scrollDirection > 0) {
                //    MM.CameraManager.ZoomIn(.0005f * scrollMagnitude);
                //} else if (scrollDirection < 0) {
                //    MM.CameraManager.ZoomOut(.0005f * scrollMagnitude);
                //}

                MM.MapManager.Update();
                MM.TileManager.Update();
                MM.EntityManager.Update();

                if (keyboardState.GetPressedKeys().Any()) {
                    foreach (var key in keyboardState.GetPressedKeys()) {
                        switch (key) {
                            case Keys.Escape:
                                Exit();
                                break;
                            case Keys.Up:
                            case Keys.W:
                                MM.CameraManager.Move(-Vector2.UnitY * 8 / MM.CameraManager.Zoom);
                                break;
                            case Keys.Down:
                            case Keys.S:
                                MM.CameraManager.Move(Vector2.UnitY * 8 / MM.CameraManager.Zoom);
                                break;
                            case Keys.Left:
                            case Keys.A:
                                MM.CameraManager.Move(-Vector2.UnitX * 8 / MM.CameraManager.Zoom);
                                break;
                            case Keys.Right:
                            case Keys.D:
                                MM.CameraManager.Move(Vector2.UnitX * 8 / MM.CameraManager.Zoom);
                                break;
                            case Keys.OemPlus:
                                MM.CameraManager.ZoomIn(.025f);
                                break;
                            case Keys.OemMinus:
                                MM.CameraManager.ZoomOut(.025f);
                                break;
                            case Keys.R:
                                if (previousKeyboardState.IsKeyUp(Keys.R)) {
                                    randomize();
                                }
                                break;
                        }
                    }
                }
            } finally {
                previousMouseState = mouseState;
                previousKeyboardState = keyboardState;
            }

            base.Update(gameTime);
        }

        private void randomize() {
            MM.MapManager.Initialize(getRandomGrassTile, tileGenerator);
            MM.EntityManager.Initialize(contentGenerator);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(new Color(47, 129, 54));

            spriteBatch.Begin(samplerState: SamplerState.PointClamp, transformMatrix: MM.CameraManager.GetViewMatrix());
            
            MM.MapManager.Draw(spriteBatch);
            MM.EntityManager.Draw(spriteBatch);

            spriteBatch.End();
            Desktop.Render();
            base.Draw(gameTime);
        }

        private void spawnAnimal(Vector2 position)
            => MM.EntityManager.AddRandomFromSet("Animals", position);

        private void spawnTree(Vector2 position)
            => MM.EntityManager.AddRandomFromSet("Trees", position);

        private void makeTile(int lakeX, int lakeY, int lakeWidth, int lakeHeight, string mainTileDefName) {
            for (int i = 0; i < lakeWidth; i++) {
                for (int j = 0; j < lakeHeight; j++) {
                    if (lakeX + i < MapManager.Width && lakeY + j < MapManager.Height) {
                        switch (mainTileDefName) {
                            case "Grass":
                                MM.MapManager[lakeX + i, lakeY + j, 0] = getRandomGrassTile(lakeX + i, lakeY + j);
                                //MM.MapManager[lakeX + i, lakeY + j, 1] = null;
                                break;
                            case "Water":
                                MM.MapManager[lakeX + i, lakeY + j, 0] = getRandomWaterTile();
                                //while (MM.MapManager[lakeX + i, lakeY + j, 0].Name == "Grass_PrettyFlower") {
                                //    MM.MapManager[lakeX + i, lakeY + j, 0] = getRandomGrassTile();
                                //}
                                break;
                            case "Path":
                                MM.MapManager[lakeX + i, lakeY + j, 0] = getRandomPathTile();
                                break;
                        }
                    }
                }
            }

            MM.MapManager.CleanUp();
        }

        void makeTile(Vector2 worldPosition, bool water, bool path)
            => makeTile(
                    (int)worldPosition.X / 32 - (water || path ? 1 : 0), 
                    (int)worldPosition.Y / 32 - (water || path ? 1 : 0), 
                    (water || path) ? 3 : 1, 
                    (water || path) ? 3 : 1, 
                    water ? "Water" : path ? "Path" : "Grass");
    }
}
