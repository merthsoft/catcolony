﻿using Microsoft.Xna.Framework.Content;
using MonoGame.Spritesheet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CatColony.Defs {
    public class DirectionalEntityDef : EntityDef {
        [JsonProperty("directionalFrames")]
        public Dictionary<string, Dictionary<string, List<FrameDef>>> DirectionalFrames { get; set; } = new Dictionary<string, Dictionary<string, List<FrameDef>>>();

        [JsonProperty("useStandardDirectionColumns")]
        public bool UseStandardDirectionColumns { get; set; }

        public override void LoadContentAndInitialize(ContentManager content) {
            GridSheet = content.Load<GridSheet>(AssetName);
            var numSpritesAcross = GridSheet.Texture.Width / GridSheet.SpriteWidth;

            if (UseStandardDirectionColumns) {
                var directionIndex = 0;
                Enum.GetNames(typeof(Direction)).ForEach(direction => {
                    StateFrames.ForEach((state, frames) => {
                        SetDirectionalFrames(direction, state, frames, numSpritesAcross, directionIndex);
                    });
                    directionIndex++;
                });
            }

            DirectionalFrames.SelectMany(x => x.Value.SelectMany(y => y.Value)).Where(frame => frame.Id.HasValue).ForEach(frame => {
                frame.SetRowAndColumnFromId(numSpritesAcross);
            });
        }

        private void SetDirectionalFrames(string direction, string state, List<FrameDef> frames, int numSpritesAcross, int directionIndex) {
            if (!DirectionalFrames.ContainsKey(state)) {
                DirectionalFrames[state] = new Dictionary<string, List<FrameDef>>();
            }
            DirectionalFrames[state][direction] = frames.SelectList(f => new FrameDef {
                Id = f.Id == -1 ? -1 : f.Id + directionIndex * numSpritesAcross * SpriteWidth,
                Delay = f.Delay,
            });
        }
    }
}
