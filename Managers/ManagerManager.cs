﻿using CatColony.Defs;
using CatColony.Entities;
using System;

namespace CatColony.Managers {
    public static class ManagerManager {
        public static MapManager MapManager { get; } = new MapManager();
        public static TileManager TileManager { get; } = new TileManager();
        public static EntityManager EntityManager { get; } = new EntityManager();
        public static GridSheetManager GridSheetManager { get; } = new GridSheetManager();
        public static ActivityManager ActivityManager { get; } = new ActivityManager();
        public static NoiseManager NoiseManager { get; } = new NoiseManager();
        public static CameraManager CameraManager { get; } = new CameraManager();
        public static RandomNumberManager RandomNumberManager { get; } = new RandomNumberManager();
    }
}
