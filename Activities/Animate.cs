﻿using CatColony.Defs;
using CatColony.Entities;

namespace CatColony.Activities {
    class Animate : Activity {
        public Entity Owner { get; set; }

        public Animate(ActivityDef def, Entity owner) : base(def) {
            Owner = owner;
        }

        public override bool Update() {
            Owner.UpdateFrame();
            return Owner.Frames[Owner.FrameIndex].Id == -1;
        }
    }
}
