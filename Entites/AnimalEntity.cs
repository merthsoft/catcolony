﻿using CatColony.Defs;
using CatColony.Managers;

namespace CatColony.Entities {
    class AnimalEntity : DirectionalEntity {
        public override bool Remove => ManagerManager.MapManager.EntityIsBlocked(this);

        public AnimalEntity(EntityDef entityDef) : base(entityDef) {
        }
    }
}
