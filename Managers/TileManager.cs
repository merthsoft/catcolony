﻿using CatColony.Defs;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System.Linq;

namespace CatColony.Managers {
    public class TileManager : IManager {
        private Dictionary<string, TileSetDef> TileSetDefs = new Dictionary<string, TileSetDef>();

        public TileDef this[string set, string tile]
            => TileSetDefs[set].Tiles.OfName(tile);

        public TileDef this[string set, int tile] 
            => TileSetDefs[set].Tiles[tile];

        public TileDef this[string combinedSetAndTileName] {
            get {
                var split = combinedSetAndTileName.Split('/');
                return this[split[0], split[1]];
            }
        }

        public void LoadContent(TileSetDef tileSetDef, ContentManager content) {
            TileSetDefs[tileSetDef.Name] = tileSetDef;

            var terrainTiles = ManagerManager.GridSheetManager.GetOrLoadContent(tileSetDef.AssetName, content);
            var numSpritesAcross = terrainTiles.Texture.Width / terrainTiles.SpriteWidth;

            tileSetDef.Tiles.ForEach(t => {
                t.Parent = tileSetDef;
                t.Frames.Where(f => f.Id.HasValue).ForEach(f => {
                    var id = f.Id.Value;
                    f.Column = id % numSpritesAcross;
                    f.Row = id / numSpritesAcross;
                });
            });
        }

        public void Update()
            => TileSetDefs.SelectMany(ts => ts.Value.Tiles).ForEach(t => t.Update());
    }
}
