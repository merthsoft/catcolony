﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CatColony.Defs {
    public class EntitySetDef : Def {
        [JsonProperty("entities")]
        public List<EntityDef> Entities { get; set; } = new List<EntityDef>();
    }
}
