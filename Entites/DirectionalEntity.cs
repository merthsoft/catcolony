﻿using CatColony.Defs;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace CatColony.Entities {
    class DirectionalEntity : Entity {
        public DirectionalEntityDef DirectionalEntityDef => (DirectionalEntityDef)EntityDef;
        public Direction Direction { get; set; }

        public override IList<FrameDef> Frames => DirectionalEntityDef?.DirectionalFrames[State][Direction.ToString()];
        
        public DirectionalEntity(EntityDef entityDef) : base(entityDef) {
        }
    }
}
