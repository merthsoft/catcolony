﻿using CatColony.Activities;
using CatColony.Defs;
using CatColony.Entities;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using static CatColony.ReflectionHelper;

namespace CatColony.Managers {
    public class ActivityManager : IManager {
        private Dictionary<string, Func<ActivityDef, Entity, Activity>> Activities = new Dictionary<string, Func<ActivityDef, Entity, Activity>>();

        public ActivityManager() {
        }

        public void LoadContent(ActivityDef a, ContentManager content)
            => Activities[a.Activity] = (ActivityDef def, Entity entity) => InvokeConstructor<Activity>(a.Activity, new object[] { def, entity });

        internal Activity Invoke(ActivityDef def, Entity entity)
            => Activities[def.Activity](def, entity);
    }
}