﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CatColony.Defs {
    public class Def {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonExtensionData]
        public IDictionary<string, JToken> ExtraFields;
    }
}
