﻿using CatColony.Defs;
using Microsoft.Xna.Framework.Graphics;

namespace CatColony.Activities {
    public abstract class Activity {
        public ActivityDef ActivityDef { get; }
        public string Title => ActivityDef.Name;

        public Activity(ActivityDef def) {
            ActivityDef = def;
        }

        public virtual void Draw(SpriteBatch spriteBatch) { }
        public abstract bool Update();
    }
}
