﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace CatColony.Managers {
    public class CameraManager : IManager {
        private OrthographicCamera Camera { get; set; }
        public float Zoom => Camera.Zoom;

        public CameraManager() {
        }

        public void Initialize(GraphicsDevice graphicsDevice) {
            Camera = new OrthographicCamera(graphicsDevice) { Position = new Vector2(3651.431f, 4065.141f), Zoom = 0.1f };
        }

        public RectangleF BoundingRectangle
            => Camera.BoundingRectangle;

        public Vector2 ScreenToWorld(int x, int y)
           => Camera.ScreenToWorld(x, y);

        internal void ZoomOut(float v)
            => Camera.ZoomOut(v);

        internal void ZoomIn(float v)
            => Camera.ZoomIn(v);

        internal void Move(Vector2 direction)
            => Camera.Move(direction);

        internal Matrix? GetViewMatrix()
            => Camera.GetViewMatrix();
    }
}