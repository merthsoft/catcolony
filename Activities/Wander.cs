﻿using CatColony.Defs;
using CatColony.Entities;
using CatColony.Managers;
using System;

namespace CatColony.Activities {
    class Wander : Activity {
        public DirectionalEntity Owner { get; set; }
        public (float X, float Y) Destination { get; set; }

        private int Distance
            => int.Parse(ActivityDef.ExtraFields.SafeGet("distance")?.ToString() ?? "100");

        private bool DestinationSet { get; set; }

        public Wander(ActivityDef def, Entity owner) : base(def) {
            Owner = owner as DirectionalEntity;

            DestinationSet = false;
            Console.WriteLine(Destination);
        }

        public override bool Update() {
            if (!DestinationSet) {
                Destination = (Owner.X + ManagerManager.RandomNumberManager.Next(-Distance, Distance), Owner.Y + ManagerManager.RandomNumberManager.Next(-Distance, Distance));
                DestinationSet = true;
            }

            if (Destination.X > Owner.X) {
                Owner.Direction = Direction.East;
            } else if (Destination.X < Owner.X) {
                Owner.Direction = Direction.West;
            } else if (Destination.Y > Owner.Y) {
                Owner.Direction = Direction.North;
            } else if (Destination.Y < Owner.Y) {
                Owner.Direction = Direction.South;
            } else {
                DestinationSet = false;
            }

            if (!StepInDirection()) {
                DestinationSet = false;
            }

            return !DestinationSet;
        }


        public bool StepInDirection()
            => StepInDirection(Owner.Direction);

        public bool StepInDirection(Direction direction) {
            var oldX = Owner.X;
            var oldY = Owner.Y;

            switch (direction) {
                case Direction.East:
                    Owner.X += Owner.DirectionalEntityDef.Speed.Value;
                    break;
                case Direction.North:
                    Owner.Y += Owner.DirectionalEntityDef.Speed.Value;
                    break;
                case Direction.South:
                    Owner.Y -= Owner.DirectionalEntityDef.Speed.Value;
                    break;
                case Direction.West:
                    Owner.X -= Owner.DirectionalEntityDef.Speed.Value;
                    break;
                default:
                    break;
            }

            if (Managers.ManagerManager.MapManager.EntityIsBlocked(Owner)) { 
                Owner.X = oldX;
                Owner.Y = oldY;
                return false;
            } else {
                Owner.UpdateFrame();
                return true;
            }
        }
    }
}
