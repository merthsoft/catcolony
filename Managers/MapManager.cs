﻿using CatColony.Defs;
using CatColony.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CatColony.Managers {
    public class MapManager : IManager {
        public static int Width = 300;
        public static int Height = 300;
        public static int Layers = 10;

        public TileDef[,,] Map { get; } = new TileDef[Width, Height, Layers];

        public TileDef this[int x, int y, int layer] {
            get {
                if (x < 0 || y < 0 || layer < 0 || x >= Width || y >= Height || layer >= Layers) { return null; }
                return Map[x, y, layer];
            }
            set {
                if (x < 0 || y < 0 || layer < 0 || x >= Width || y >= Height || layer >= Layers) { return; }
                Map[x, y, layer] = value;
            }
        }

        public void Initialize(Func<int, int, TileDef> tileGenerator, Func<IEnumerable<(int x, int y, TileDef)>> lakeGenerator) {
            ManagerManager.NoiseManager.Initialize();
            Map.Initialize(0, tileGenerator);
            foreach (var (x, y, tile) in lakeGenerator()) {
                Map[x, y, 0] = tile;
            }

            CleanUp();
        }

        public void ForEach(Action<int, int, int, TileDef> function, int startIndexX, int startIndexY, int endIndexXOffset, int endIndexYOffset)
            => Map.ForEach(function, startIndexX, startIndexY, endIndexXOffset, endIndexYOffset);

        public void Draw(SpriteBatch spriteBatch) {
            var viewRectF = ManagerManager.CameraManager.BoundingRectangle;
            var xStart = (int)(viewRectF.X / 32) - 5;
            var yStart = (int)(viewRectF.Y / 32) - 5;
            var width = (int)(viewRectF.Width / 32) + 5;
            var height = (int)(viewRectF.Height / 32) + 5;

            for (int y = yStart; y < yStart + height; y++) {
                for (int x = xStart; x < xStart + width; x++) {
                    var tileDef = this[x, y, 0];
                    if (tileDef == null) { continue; }
                    var frame = tileDef.Current;
                    var terrainTiles = ManagerManager.GridSheetManager[tileDef.Parent.AssetName];
                    spriteBatch.Draw(terrainTiles.Texture, new Vector2(x * 32, y * 32), terrainTiles[frame.Column, frame.Row], Color.White);
                }
            }
        }

        public void Update() {
            for (int y = 0; y < Height; y++) {
                for (int x = 0; x < Width; x++) {
                    var tileDef = Map[x, y, 0];
                    if (tileDef?.Name == "Water" && ManagerManager.RandomNumberManager.NextDouble() < .00025) {
                        ManagerManager.EntityManager.Add("Effects", "Water_Shimmer", x * 32 + 16, y * 32 + 16);
                    }
                }
            }
        }

        public bool EntityIsBlocked(Entity entity) {
            for (int i = 0; i < entity.EntityDef.SpriteWidth; i++) {
                for (int j = 0; j < entity.EntityDef.SpriteHeight; j++) {
                    if (TileIsBlocking((int)(entity.X + entity.EntityDef.SpriteWidth * 16) / 32 + i, (int)(entity.Y + entity.EntityDef.SpriteHeight * 16) / 32 + j)) {
                        return true;
                    }
                }
            }

            return false;
        }

        internal bool TileIsBlocking(float x, float y)
            => TileIsBlocking((int)(x / 32), (int)(y / 32));

        internal bool TileIsBlocking(int x, int y)
            => (x > 0 && x < Width
                && y > 0 && y < Height)
            ? Enumerable.Range(0, Layers).Any(layer => this[x, y, layer]?.IsBlocking ?? false)
            : true;

        public void CleanUp() {
            const int layer = 0;
            for (int y = 0; y < Height; y++) {
                for (int x = 0; x < Width; x++) {
                    var tileDef = Map[x, y, 0];
                    var tileName = tileDef?.Parent.Name;
                    if (tileName == "Water" || tileName == "Path") {
                        var grassLeft = this[x - 1, y, layer]?.Parent.Name != tileName;
                        var grassRight = this[x + 1, y, layer]?.Parent.Name != tileName;
                        var grassAbove = this[x, y - 1, layer]?.Parent.Name != tileName;
                        var grassBelow = this[x, y + 1, layer]?.Parent.Name != tileName;

                        var grassLowerRight = this[x + 1, y + 1, layer]?.Parent.Name != tileName;
                        var grassUpperRight = this[x + 1, y - 1, layer]?.Parent.Name != tileName;
                        var grassLowerLeft = this[x - 1, y + 1, layer]?.Parent.Name != tileName;
                        var grassUpperLeft = this[x - 1, y - 1, layer]?.Parent.Name != tileName;

                        if (grassLeft && grassRight && grassAbove && grassBelow && tileName == "Water") {
                            this[x, y, layer] = ManagerManager.TileManager[tileName, $"Puddle_2"];
                        } else if (grassLeft) {
                            if (grassAbove) {
                                this[x, y, layer] = ManagerManager.TileManager[tileName, "UpperLeft_Grass"];
                            } else if (grassBelow) {
                                this[x, y, layer] = ManagerManager.TileManager[tileName, "LowerLeft_Grass"];
                            } else {
                                this[x, y, layer] = ManagerManager.TileManager[tileName, "Left_Grass"];
                            }
                        } else if (grassRight) {
                            if (grassAbove) {
                                this[x, y, layer] = ManagerManager.TileManager[tileName, "UpperRight_Grass"];
                            } else if (grassBelow) {
                                this[x, y, layer] = ManagerManager.TileManager[tileName, "LowerRight_Grass"];
                            } else {
                                this[x, y, layer] = ManagerManager.TileManager[tileName, "Right_Grass"];
                            }
                        } else if (grassAbove) {
                            this[x, y, layer] = ManagerManager.TileManager[tileName, "Upper_Grass"];
                        } else if (grassBelow) {
                            this[x, y, layer] = ManagerManager.TileManager[tileName, "Lower_Grass"];
                        } else if (grassUpperRight) {
                            this[x, y, layer] = ManagerManager.TileManager[tileName, "UpperRight_Grass_Island"];
                        } else if (grassLowerLeft) {
                            this[x, y, layer] = ManagerManager.TileManager[tileName, "LowerLeft_Grass_Island"];
                        } else if (grassUpperLeft) {
                            this[x, y, layer] = ManagerManager.TileManager[tileName, "UpperLeft_Grass_Island"];
                        } else if (grassLowerRight) {
                            this[x, y, layer] = ManagerManager.TileManager[tileName, "LowerRight_Grass_Island"];
                        }
                    }
                }
            }
        }
    }
}
