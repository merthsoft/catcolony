﻿using System;

namespace CatColony {
    public static class Program {
        [STAThread]
        static void Main() {
            using var game = new ColonySimulatorGame();
            game.Run();
        }
    }
}
