﻿using Microsoft.Xna.Framework.Content;
using MonoGame.Spritesheet;
using System;
using System.Collections.Generic;

namespace CatColony.Managers {
    public class GridSheetManager : IManager {
        private Dictionary<string, GridSheet> Sheets { get; } = new Dictionary<string, GridSheet>();

        public GridSheetManager() {
        }

        public GridSheet this[string sheetName] {
            get => Sheets[sheetName];
            set => Sheets[sheetName] = value;
        }

        public GridSheet GetOrLoadContent(string assetName, ContentManager content) {
            if (!Sheets.ContainsKey(assetName)) {
                Sheets[assetName] = content.Load<GridSheet>(assetName);
            }

            return Sheets[assetName];
        }
    }
}