﻿using CatColony.Activities;
using CatColony.Defs;
using CatColony.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Linq;

namespace CatColony.Entities {
    public class Entity {
        public EntityDef EntityDef { get; }

        float x;
        public float X {
            get {
                return x;
            }
            set {
                x = value;
                if (EntityDef.SnapToGrid.Value) {
                    x = (int)(x / 32) * 32;
                }
            }
        }

        float y;
        public float Y {
            get {
                return y;
            }
            set {
                y = value;
                if (EntityDef.SnapToGrid.Value) {
                    y = (int)(y / 32) * 32;
                }
            }
        }

        public Activity Activities { get; set; }

        public virtual bool Remove => Frames[FrameIndex].Id == -2;
        public virtual string Name => "";
        public virtual string State { get; set; }
        public int FrameIndex { get; set; }

        public virtual IList<FrameDef> Frames => EntityDef.StateFrames[State];

        protected int tickCount;
        protected int lastFrameTickCount;
        protected int loopCount;

        public Entity(EntityDef entityDef) {
            EntityDef = entityDef;
            Activities = new ActivityContainer(this, EntityDef.Activities.Select(def => (ManagerManager.ActivityManager.Invoke(def, this), def.Weight.Value)).ToArray());
        }

        public virtual void Draw(SpriteBatch spriteBatch) {
            var spriteSheet = EntityDef.GridSheet;
            var sprite = spriteSheet[Frames[FrameIndex].Column, Frames[FrameIndex].Row];
            var spriteWidth = EntityDef.SpriteWidth.Value;
            var spriteHeight = EntityDef.SpriteHeight.Value;
            sprite = sprite.Clone(width: spriteWidth * 32, height: spriteHeight * 32);
            spriteBatch.Draw(spriteSheet.Texture, new Vector2(X, Y), sprite, Color.White);
        }

        public virtual void Update() {
            Activities.Update();
        }

        public virtual void UpdateFrame() {
            tickCount++;
            if (tickCount - lastFrameTickCount >= Frames[FrameIndex].Delay) {
                lastFrameTickCount = tickCount;
                FrameIndex++;
                if (FrameIndex == EntityDef.StateFrames[State].Count) {
                    FrameIndex = 0;
                    loopCount++;
                }
            }
        }
    }
}
