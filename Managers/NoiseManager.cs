﻿using SharpNoise;
using SharpNoise.Builders;
using SharpNoise.Modules;
using System.Collections.Generic;
using System.Linq;

namespace CatColony.Managers {
    public class NoiseManager : IManager {
        private Dictionary<int, (int x, int y)[]> PeakCache { get; } = new Dictionary<int, (int x, int y)[]>();
        private Dictionary<int, (int x, int y)[]> TroughCache { get; } = new Dictionary<int, (int x, int y)[]>();

        private float[,,] noiseValues;
        private float[,,] NoiseValues {
            get {
                return noiseValues ??= generateValues();
            }
        }

        public int Seed { get; set; } = 0;
        public double Frequency { get; set; } = .25;
        public double Lacunarity { get; set; } = 2.5;
        public int OctaveCount { get; set; } = 6;
        public double Persistence { get; set; } = .1;

        private float[,,] generateValues() {
            var noiseSource = new Perlin {
                Seed = Seed,
                Frequency = Frequency,
                Lacunarity = Lacunarity,
                OctaveCount = OctaveCount,
                Persistence = Persistence,
                Quality = NoiseQuality.Best,
            };
            var noiseMap = new NoiseMap();
            var noiseMapBuilder = new PlaneNoiseMapBuilder {
                DestNoiseMap = noiseMap,
                SourceModule = noiseSource
            };
            noiseMapBuilder.SetDestSize(MapManager.Width, MapManager.Height);
            noiseMapBuilder.SetBounds(6, 10, 1, 5);
            noiseMapBuilder.Build();

            var min = float.MaxValue;
            var max = float.MinValue;

            var minLocations = new List<(int x, int y)>();
            var maxLocations = new List<(int x, int y)>();

            var ret = new float[MapManager.Width, MapManager.Height, 1];
            var index = 0;
            for (int j = 0; j < MapManager.Height; j++) {
                for (int i = 0; i < MapManager.Width; i++) {
                    var v = noiseMap.Data[index++];
                    if (v <= min) {
                        if (v < min) {
                            minLocations.Clear();
                        }
                        min = v;
                        minLocations.Add((i, j));
                    } else if (v >= max) {
                        if (v > max) {
                            maxLocations.Clear();
                        }
                        max = v;
                        maxLocations.Add((i, j));
                    }

                    ret[i, j, 0] = v;
                }
            }

            //var oldMin = min;
            //var oldMax = max;
            //
            //min = float.MaxValue;
            //max = float.MinValue;
            //
            //minLocations = new List<(int x, int y)>();
            //maxLocations = new List<(int x, int y)>();

            //index = 0;
            //for (int j = 0; j < MapManager.Height; j++) {
            //    for (int i = 0; i < MapManager.Width; i++) {
            //        var v = noiseMap.Data[index++].Map(oldMin, -1, oldMax, 1);
            //        ret[i, j, 0] = v;
            //        if (v <= min) {
            //            if (v < min) {
            //                minLocations.Clear();
            //            }
            //            min = v;
            //            minLocations.Add((i, j));
            //        } else if (v >= max) {
            //            if (v > max) {
            //                maxLocations.Clear();
            //            }
            //            max = v;
            //            maxLocations.Add((i, j));
            //        }
            //    }
            //}

            PeakCache[0] = maxLocations.ToArray();
            TroughCache[0] = minLocations.ToArray();
            
            return ret;
        }

        public float this[int x, int y, int layer] {
            get {
                if (x < 0 || y < 0
                    || x >= MapManager.Width || y >= MapManager.Height) {
                    return 0;
                }
                return NoiseValues[x, y, layer];
            }
        }

        public float this[(int x, int y) pair, int layer]
            => this[pair.x, pair.y, layer];

        public IEnumerable<(int x, int y)> FindPeaksAtLayer(int layer)
            => PeakCache[layer].Select(v => (v.x, v.y));

        public IEnumerable<(int x, int y)> GetChunkAt(int x, int y, int layer, float min, float max)
            => GetChunkAt((x, y), layer, min, max);

        public IEnumerable<(int x, int y)> GetChunkAt((int x, int y) seed, int layer, float min, float max) {
            HashSet<(int x, int y)> ret = new HashSet<(int x, int y)>();
            HashSet<(int x, int y)> visitedCells = new HashSet<(int x, int y)>();
            Stack<(int x, int y)> cells = new Stack<(int x, int y)>();
            cells.Push(seed);

            while (cells.TryPop(out var cell)) {
                if (visitedCells.Contains(cell)) { continue; }
                visitedCells.Add(cell);
                if (this[cell, layer] > min && this[cell, layer] < max) {
                    ret.Add(cell);
                    cells.Push((cell.x - 1, cell.y - 1));
                    cells.Push((cell.x - 1, cell.y));
                    cells.Push((cell.x - 1, cell.y + 1));


                    cells.Push((cell.x, cell.y - 1));
                    cells.Push((cell.x, cell.y + 1));

                    cells.Push((cell.x + 1, cell.y - 1));
                    cells.Push((cell.x + 1, cell.y));
                    cells.Push((cell.x + 1, cell.y + 1));
                }
            }

            return ret;
        }

        public IEnumerable<(int x, int y)> FindTroughsAtLayer(int layer)
            => TroughCache[layer].Select(v => (v.x, v.y));

        public void Initialize() {
            noiseValues = generateValues();
        }
    }
}
