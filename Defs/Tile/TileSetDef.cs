﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CatColony.Defs {
    public class TileSetDef : Def {
        [JsonProperty("assetName")]
        public string AssetName { get; set; }

        [JsonProperty("tiles")]
        public List<TileDef> Tiles { get; set; }
    }
}
